import random
import time


class Creature:
    # Properties of a Creature
    name = None
    hp = None
    inventory = []  # What a creature is holding
    weapon = None
    inventory_capacity = None
    room_pos = None  # Which room the creature is in
    # Flag for if the creature can take advantage of magic weapons.
    magic = False
    magic_dampened = False

    # Getters and Setters
    def get_name(self):
        return self.name

    def get_hp(self):
        return self.hp

    def equip_weapon(self, selected_weapon):
        self.weapon = selected_weapon

    def equip_armor(self, selected_armor):
        self.armor = selected_armor

    def get_weapon(self):
        return self.weapon

    def get_armor(self):
        return self.armor

    # This is how 2 creatures will fight.
    def fight(self, enemy):
        # Get the min and max attack from the weapon the creature hs equipped
        min_attack = self.weapon.get_min_attack()
        max_attack = self.weapon.get_max_attack()

        # Get the speed, which is the number of attacks per turn a creature can make.
        speed = self.weapon.get_speed()

        # Check the armor of the enemy.
        defence = enemy.armor.get_defence()

        while self.is_alive() and enemy.is_alive():
            print(self.name, "'s turn")
            for _ in range(speed):
                # Roll for damage between the min and max attack stats.
                damage = random.randint(min_attack, max_attack)
                # Humans get to use magic weapons.
                if isinstance(self, Human):
                    # So  long as the weapon has magic and the Human isn't being magic dampened by the room.
                    if self.weapon.magic and not self.magic_dampened:
                        # Damgage increase.
                        damage *= 1.5
                        print("Attack increased by magic!")
                # Once the damage has been deteremined, it's checked against the enemy defence.
                if damage > defence:
                    print(self.name, "attacked with", self.weapon.get_name(),
                          "dealing", damage, "points of damage.")
                    # When the damage is higher, the enemy is given the damage to subtact from their hp.
                    enemy.takeDamage(damage)
                else:
                    # If the damage is lower than the defence, then it is negated.
                    print("The attack bounced of", str(enemy.get_name()) +
                          "'s", enemy.armor.get_name(), "causing no damage!")
                # Give the current HP after the current attack
                print(str(enemy.get_name())+"'s",
                      "current HP: ", enemy.get_hp())
                print()
                # Gives the player time to read the text, so it's not rushing past them.
                time.sleep(3)
            if(enemy.is_alive()):
                # Move to the enemy's turn.
                enemy.fight(self)

    # Takes the damage from figh() and subtracts it from the hp.
    def takeDamage(self, damage: int):
        self.hp -= damage

        # It doesn't look to good to go below zero, so this will stop that.
        if self.hp < 0:
            self.hp = 0

    # Returns the names of the itmes in a creature's inventory.
    def get_inventory_names(self):

        item_list = []
        for item in (self.inventory):
            item_list.append(item.name)
        return item_list

    # This returns not just the names, but the actual inventory.
    def get_inventory_list(self):
        return self.inventory

    # More Getters and Setters
    def get_inventory_cap(self):
        return self.inventory_capacity

    def set_inventory_cap(self, new_cap):
        self.inventory_capacity = new_cap

    def is_inventory_full(self):
        return (len(self.inventory)) >= self.inventory_capacity

    def put_in_inventory(self, item):
        self.inventory.append(item)

    def take_from_inventory(self, selection):
        return self.inventory.pop(selection)

    # Determine if the creature is still alive.
    def is_alive(self):
        return self.hp > 0


class Human(Creature):
    def __init__(self, new_name):
        # Humans start with 1000 hp since there's mulitple enemies to fight.
        self.hp = 1000
        self.name = new_name

        # Humans start off with non magical sword.
        self.weapon = Sword("Sword", False)

        # Humans start off with a bag capacity of 4
        self.inventory_capacity = 4
        self.inventory = []

        # Start with the sword in the inventory.
        self.inventory.append(self.weapon)

        # Humans wear armor.
        self.armor = Human_armor("Iron Armor")
        self.room_pos = 0

        # Humans have five potions they can use.
        self.potions = 5
        self.gameover = False

        # If this is set to True, then the human can't get a boost from magic weapons.
        self.magic_dampened = False

        # Wallet which holds gold.
        self.wallet = 0

        # Monster keys to leave the dungeon.
        self.monster_keys = 0

    def give_key(self):
        self.monster_keys += 1

    # More getters and setters.

    def get_keys(self):
        return self.monster_keys

    def set_magic_dampened(self, flag):
        self.magic_dampened = flag

    def add_to_wallet(self, amount):
        self.wallet += amount

    def look_in_wallet(self):
        return self.wallet

    def move(self, selection):
        self.room_pos = selection

    # This will recover the player's hp and decrease the number of potions.
    # I've used sleep here to give the player time to read.
    def use_potion(self):
        self.hp += 300  # Player is healed by 300

        # Won't allow player hp to go above 1000
        if self.hp > 1000:
            self.hp = 1000

        time.sleep(1)
        print("Potion used.  HP recovered by 300 points")
        time.sleep(1)
        print("Current HP: ", self.hp)
        time.sleep(1)
        self.potions -= 1  # Player now has one less potion

    # Getters and setters.
    def get_potions(self):
        return self.potions

    def set_gameover(self):
        self.gameover = True

    def has_gameover(self):
        return self.gameover


class Monster(Creature):
    def __init__(self, new_name):
        # Monsters have 100 hp since there's going to be many of them.
        self.hp = 100
        self.name = new_name

        # They have claws to attack with.
        self.weapon = Claws("Claws")
        self.inventory_capacity = 4
        self.inventory = []
        self.inventory.append(self.weapon)

        # They have hide that protects them.
        self.armor = Hide("Hide")


class Weapon:
    name = None
    min_attack = None
    max_attack = None
    speed = None

    # Getters for basic stats
    def get_name(self):
        return self.name

    def get_min_attack(self):
        return self.min_attack

    def get_max_attack(self):
        return self.max_attack

    def get_speed(self):
        return self.speed


class Sword(Weapon):

    def __init__(self, new_name, magic_flag):
        self.name = new_name

        # Swords have an attack range between 4 and 8
        self.min_attack = 4
        self.max_attack = 8

        # They can only attack once per turn.
        self.speed = 1

        # They can be magical.
        self.magic = magic_flag


class Dagger(Weapon):

    def __init__(self, new_name, magic_flag):
        self.name = new_name

        # Daggers have an attack range between 1 and 4
        self.min_attack = 1
        self.max_attack = 4

        # They can attack four times per turn.
        self.speed = 4

        # Daggers can be magical.
        self.magic = magic_flag


class Claws(Weapon):

    def __init__(self, new_name):
        self.name = new_name

        # Claws have an attack range of 3 to 4.
        self.min_attack = 3
        self.max_attack = 4

        # Claws can attack twice per turn.
        self.speed = 2


class Armor:
    defence = None

    # Getter for defence stat.
    def get_defence(self):
        return self.defence


class Human_armor(Armor):
    def __init__(self, new_name):
        # Human armor has defence between 1 and 3.
        self.defence = random.randint(1, 3)
        self.name = new_name

    # Getters and setters.
    def get_name(self):
        return self.name

    def get_defence(self):
        return self.defence

    def set_defence(self, new_def):
        self.defence = new_def


class Hide(Armor):
    def __init__(self, new_name):
        # Human armor has defence between 1 and 4.
        self.defence = random.randint(1, 4)
        self.name = new_name

    # Getters and setters.
    def get_name(self):
        return self.name

    def get_defence(self):
        return self.defence


class Room:
    room_num = 0  # Identifies the room.
    magic_dampening = None  # Whether or not the room dampens magic tools.
    room_treasure = None  # The amount of gold in the room.

    def __init__(self, new_room_num, new_adj):
        self.room_num = new_room_num

        # The amount of gold found in the room.
        self.room_treasure = random.randint(1, 100)

        self.on_floor = []  # A list of items lying on the floor.
        self.monster_flag = True  # Rooms always start with monster.

        self.monster = None  # Where the monster is stored.

        # The adjacent rooms, the constructor adds to the list upon creation.
        self.adj_rooms = []
        self.adj_rooms.extend(new_adj)

    # Getters and setters.

    def create_monster(self, new_name):
        self.monster = Monster(new_name)

    def get_room_num(self):
        return self.room_num

    def get_adj(self):
        return self.adj_rooms

    def set_magic_dampening(self, magic_dampening_flag):
        self.magic_dampening = magic_dampening_flag

    def is_magic_dampening(self):
        return self.magic_dampening

    def has_monster(self):
        return self.monster_flag

    def set_monster_flag(self, flag):
        self.monster_flag = flag

    def get_treasure(self):
        return self.room_treasure

    def set_on_floor(self, item_dropped):
        self.on_floor.append(item_dropped)

    def look_at_floor(self):
        return self.on_floor

    # Return a list of names of the items on the floor.
    def look_at_floor_names(self):
        item_list = []
        for item in (self.on_floor):
            item_list.append(item.name)
        return item_list

    def take_off_floor(self, selection):
        # Pop the item at the index the user chose.
        # Subtracting 1 so the user can pick the item index as they see it.
        return self.on_floor.pop(selection - 1)
