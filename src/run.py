
import array as arr
import random
from entities import *


def generate_rooms():
    # Create 8 rooms with hard coded adjacencies.
    # This is also where a room's loot is set.

    rooms = []  # The array for holding the rooms

    # List of monster names that I got from an online generator.
    monster_names = ["Warpcat", "Horrorsword", "Phantomman", "Phantomboy",
                     "Grumpy Howler", "False Thing", "Ternal Brute", "Ghost Bee",
                     "Black-Eyed Boulder Hawk",
                     "Taloned Blight Cat"]

    # This is the entry room, so no monster.
    rooms.append(Room(0, [1, 2]))
    rooms[0].monster_flag = False

    # Rooms 1 - 7 will have monsters and can have items on the floor.
    rooms.append(Room(1, [2, 4]))
    rooms[1].set_on_floor(Human_armor("Steel Armor"))
    rooms[1].on_floor[0].set_defence(4)

    rooms.append(Room(2, [3, 5]))
    rooms[2].set_on_floor(Sword("Blinkstrike", True))

    rooms.append(Room(3, [4, 6]))
    rooms.append(Room(4, [5, 7]))

    rooms.append(Room(5, [6, 0]))
    rooms[5].set_on_floor(Dagger("Swiftly", True))

    rooms.append(Room(6, [7, 1]))
    rooms[6].set_on_floor(Sword("Dawnbreaker", True))

    rooms.append(Room(7, [6, 0]))

    # Set a random selection of room to be magic dampening.
    for room in rooms:
        # Would have used random.choice here, but getrandbits() is about a third of the exec time.
        magic_dampening_flag = bool(random.getrandbits(1))

        room.set_magic_dampening(magic_dampening_flag)
        print("room number: ", room.get_room_num(),
              "Magic", room.is_magic_dampening())

    # Generate the monster in each room and use a random name from the list of monster names.
    for room in rooms:
        if(room.has_monster()):
            room.create_monster(random.choice(monster_names))

    # Return the rooms that have been generated.
    return rooms

# This is a function because I may have to display numbered list contents often.


def display_lst_selection(lst):
    for item in lst:
        # Display the index fo the item + 1 so the player can pick non zero based selection.
        print("(", lst.index(item) + 1, ")", item.get_name())


def play(player, rooms):
    # Give the player their info.
    print("Player Name: ", player.get_name())
    print("HP: " + str(player.hp))
    pos = int(player.room_pos)
    print("Current Room: ", pos)

    # Check if the room dampens magic and if it has a monster.
    magic_dampen = rooms[pos].is_magic_dampening()
    has_monster = rooms[pos].has_monster()

    if(magic_dampen == True):
        # Player's magic weapons are dampened.
        print("Magic weapons are being weakened here...")
        player.set_magic_dampened(True)
    else:
        # Player's magic weapons are not dampened.
        player.set_magic_dampened(False)
    if(has_monster):
        # Notify the player of the monster.
        print("There's a monster here!")
        print("It's a", rooms[pos].monster.get_name(), "!")
        print("Select a weapon from your bag")

        # Put the player's inventory into a bag list.
        bag = player.get_inventory_list()

        # Call the function to display the bag.
        display_lst_selection(bag)

        # Here the player picks the item they want.
        # Accounting for a non zero based selection with "- 1"
        selection = int(input("Weapon Number: ")) - 1

        # Catching if there's an out of bounds choice given.
        # Using sleep to give the player time to read.
        # This will also catch if the choice was in their inventory, but isn't a weapon.
        while not isinstance(selection, int) or selection > len(bag) - 1 or not issubclass(type(bag[selection]), Weapon):
            time.sleep(1)
            print("That's not a weapon...")
            time.sleep(1)
            display_lst_selection(bag)
            selection = int(input("Weapon Number: ")) - 1  # Asks again.

        # Finally, the weapon is equipped.
        player.equip_weapon(player.inventory[selection])

        # The fight begins.
        player.fight(rooms[pos].monster)

        # After the fight has finished.
        if player.is_alive():
            print("Player won the fight.")

            # Give the player a monster key.
            player.give_key()
            print("You recieved a monster key")

            # The monster is dead, so the player won't have to fight it again if they come back
            rooms[pos].set_monster_flag(False)

            # Reward player with gold found in the room.
            print("You found gold!")
            gold = rooms[pos].get_treasure()
            print("You've aquired", gold, "gold coins")
            player.add_to_wallet(gold)
            print("Your current wallet: ", player.look_in_wallet())
            time.sleep(3)  # Give player time to read the message.

            # Check if the player has any potions left.
            if(player.get_potions() > 0):
                print()  # Blank line for formating.

                # Give the player their hp and ask if they want to use a potion to heal.
                print("Current HP: ", player.get_hp())
                print("Do you want to use a potion to heal by 300 points?")
                selection = int(input(" (1) Yes\n (2) No\n Answer: "))

                # Making sure the player enters 1 or 2.
                while selection not in [1, 2]:
                    print("Do you want to use a potion to heal by 300 points?")
                    # Asks again.
                    selection = int(input(" (1) Yes\n (2) No\n Answer: "))

                if selection == 1:
                    player.use_potion()
                    if player.get_potions() == 0:
                        print("You've run out of potions")
        else:
            # The player died, so it's game over.
            print("Game Over")
            return

    # If this is the last room and the player has defeated the all the monsters, then they win.
    if pos == 7 and rooms[pos].has_monster() == False and player.get_keys == 7:
        player.set_gameover()
        print("You've reached the end")
        print("You are now leaving the dungeon")
        return

    # This is where the player looks at the items in the room.
    if rooms[pos].look_at_floor():
        print("On the room's floor: ")

        # Grabs the names of the items on the floor.
        print(rooms[pos].look_at_floor_names())
        print("Do you want take anything?")
        selection = int(input(" (1) Yes\n (2) No\n Answer: "))  # Asks again.

        if selection == 1:
            print("If you already have 4 items, you'll have to leave one.")
            print("Your current bag: ", player.get_inventory_names())

            # Display the item selection menu.
            floor = rooms[pos].look_at_floor()
            display_lst_selection(floor)

            # Ask player for desired Item number.
            print("Which item do you want to pick up.")
            selection = int(input("Item Number: ")) - 1

            # Catching if there's an out of bounds choice given.
            # Accounting for non zero based selection with "-1"
            # Using sleep to give the player time to read.
            while selection > len(rooms[pos].look_at_floor()) - 1:
                time.sleep(1)
                print("That's not a choice...")
                time.sleep(1)
                display_lst_selection(rooms[pos].look_at_floor())
                selection = int(input("Item Number: ")) - 1

            if len(player.get_inventory_list()) < 4:
                player.put_in_inventory(rooms[pos].take_off_floor(selection))
                time.sleep(1)
                print("Your current bag: ", player.get_inventory_names())
                time.sleep(2)
                print(rooms[pos].look_at_floor_names())
                time.sleep(2)
            else:
                print("Which item will you leave?")

                # Get the player's bag.
                bag = player.get_inventory_list()

                # Call the function to display the bag.
                display_lst_selection(bag)

                # The player picks the itme they want to leave.
                # Accounting for a non zero based selection with "- 1"
                toss_selection = int(input("Item Number: ")) - 1
                while toss_selection > len(bag) - 1:
                    time.sleep(1)
                    print("That's not a choice...")
                    time.sleep(1)
                    display_lst_selection(bag)

                    # Ask again
                    print("Which item will you leave?")
                    toss_selection = int(input("Item Number: ")) - 1

                print("Picking up item and leaving old one.")

                # Pick up the item and put in inventory.
                player.put_in_inventory(rooms[pos].take_off_floor(selection))
                time.sleep(3)  # Give player time to read.

                # Put old item on the floor.
                rooms[pos].set_on_floor(
                    player.take_from_inventory(toss_selection))
                print("Current Bag: ", player.get_inventory_names())
                time.sleep(3)  # Give player time to read.

    # Display the current rooms that the player can access.
    adj_rooms = rooms[pos].get_adj()
    print("Connected Rooms", adj_rooms)

    # Ask the player for the next room they want to go to.
    print("Choose the room you'd like to go and press enter: ")
    selection = int(input("Room Number: "))

    # Just in case they give an unavialble room.
    # This will keep them in the loop until they pick from the available connected rooms.
    # Again, using sleep() here to give player time to read.
    while selection not in adj_rooms:
        time.sleep(1)
        print("That's not a valid room at the moment.")
        time.sleep(1)
        print("Connected Rooms", adj_rooms)
        time.sleep(1)
        selection = int(input("Room Number: "))
    player.move(selection)


def main():
    # Call the function to make the rooms.
    rooms = generate_rooms()

    # Description of thegame.
    print("Welcome to the dungeon.  There are 7 rooms besides this one.")
    print("Each room can take to two others.")
    print("The will be monsters that appear.  You only have to defeat them once.")
    print("However, if you leave the dungeon or fall in battle, you will have to start over.")
    print("You will start with a sword and iron armor.")
    print("These take up 2 slots in your 4 slot inventory.")
    print("You can pick up other items as you traverse the dungeon.")
    print("To win, you must defeat all the monsters and collect thier keys.")
    print("The keys unlock the exit in room 7.  Go there once you have all the keys")
    print()

    # Create the player.
    entered_name = input("Enter your name: ")
    player = Human(entered_name)

    # While the player is still alive and hasn't had a game over.
    while not player.has_gameover() and player.is_alive():
        play(player, rooms)


if __name__ == '__main__':
    main()
